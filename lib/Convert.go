package lib

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/spf13/viper"
)

func Convert() {
	var files []string
	err := filepath.Walk(viper.GetString("convert.input"), func(path string, info os.FileInfo, err error) error {
		files = append(files, info.Name())
		return nil
	})
	if err != nil {
		panic(err)
	}
	for x, file := range files {
		//first step is to split video file into segments, to save all the jiggybytes we won't turn it into raw until its local on client
		fmt.Printf("Splitting %v which is %v of %v\n", file, x, len(files))
		cmd := exec.Command(viper.GetString("convert.ffmpeg"), "-i " + viper.GetString("convert.input") + file + " -c copy -an - map 0 - segment_time " + viper.GetString("command.partseconds") + " -o " + viper.GetString("convert.output") + strings.TrimSuffix(file, filepath.Ext(file)) + "$04d.mkv")
		cmd.Run()
		fmt.Println("Finished!")
		os.Rename(viper.GetString("convert.input") + file, viper.GetString("convert.output") + file)
		fmt.Println("moved " + file + " to " + viper.GetString("convert.output") + file)
		//this command won't be used here, i'm just keeping it here so i can reference it later...
		//cmd := exec.Command("ffmpeg -i " + file + " -c:v rawvideo -pix_fmt yuv420p -map 0 -segment_time " + viper.GetString("command.partseconds") + " -o " + file + "%04d.y4m")
		//fmt.Println(strings.TrimSuffix(file, filepath.Ext(file)))
	}
}