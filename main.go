package main

import (
	"fmt"

	L "./lib"
	"github.com/spf13/viper"
)

func main() {

	//load config
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()

	//panic at the config
	if err != nil {
		panic(fmt.Errorf("Error Reading config file: %s \n", err))
	}
	go L.Convert()
	L.Startserver()

}
